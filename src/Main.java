public class Main {
    public static final String currDir = System.getProperty("user.dir");
    public static final String ACCOUNT_FILE_NAME = currDir + "/src/Accounts/Accounts.txt";

    public static void main(String[] args) {
        AccountManagement.createAccount();
    }
}
