
import javax.swing.*;
import java.io.*;
import java.io.FileReader;
import java.lang.reflect.Field;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class AccountManagement {
    public static void createAccount() {
        Scanner scanner = new Scanner(System.in);
        Account newAccount = new Account();
        System.out.println("Enter username: ");
        newAccount.userName = scanner.nextLine();
        while (!isUsernameValid(newAccount.userName)) {
            System.out.println("Enter username: ");
            newAccount.userName = scanner.nextLine();
        }
        System.out.println("Enter password: ");
        newAccount.password = scanner.nextLine();
        System.out.println("Enter first name: ");
        newAccount.firstName = scanner.nextLine();
        System.out.println("Enter last name: ");
        newAccount.lastName = scanner.nextLine();
        newAccount.accountNumber = String.format("%04d", AccountManagement.findGreatestAccountNumber());
        newAccount.accountType = "customer";
        writeAccount(newAccount);
        System.out.println("Account added successfully");
    }

    public static boolean isUsernameValid(String username) {
        boolean isUserNameExist = true;
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(Main.ACCOUNT_FILE_NAME))) {
            String lineFromFile = bufferedReader.readLine();
            while (lineFromFile != null) {
                if (lineFromFile.contains("UserName") && (lineFromFile.split(": ")[1].equals(username))) {
                    isUserNameExist = false;
                    break;
                }
                lineFromFile = bufferedReader.readLine();

            }
            bufferedReader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            return isUserNameExist;
        }
    }


    public static int findGreatestAccountNumber() {
        int maxAccountNumber = 0;
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(Main.ACCOUNT_FILE_NAME))) {
            String lineFromFile = bufferedReader.readLine();
            while (lineFromFile != null) {
                if (lineFromFile.contains("AccountNumber")) {
                    int currAccount = Integer.parseInt(lineFromFile.split(": ")[1]);
                    if (currAccount > maxAccountNumber) {
                        maxAccountNumber = currAccount;
                    }
                }
                lineFromFile = bufferedReader.readLine();
            }
            bufferedReader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {

            return maxAccountNumber + 1;
        }
    }

    public static void writeAccount(Account account) {
        try(BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(Main.ACCOUNT_FILE_NAME, true))) {
            bufferedWriter.write("----------\n");
            Field[] fields = account.getClass().getDeclaredFields();
            for(Field f: fields) {
                String fieldName = f.getName();
                bufferedWriter.write(fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1) + ": " + f.get(account).toString() + "\n");
            }
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
